# In[22]:


def max_sum_array(arr):
    ans = []
    max_sum = arr[0]
    start = 0
    end = 0
    n = len(arr)
    print(n)
    for i in range(1,n+1):
        #create 1 to n size subaray
       
        for j in range(0,n):
            #for iterating elements of an array
            sum = 0
            flag = 0 
            #if all numbers are negative should not return 0
            else :
                flag = 1
                for k in range(j,j+i):
                    #do sum
                    sum = sum + arr[k]
            if( sum > max_sum):
                max_sum = sum
                start = j;
                end = j+i-1;
               
    ans.append(max_sum)
    ans.append(start)
    ans.append(end)
    return ans

arr = [1,2,-3,-4,-2,-4]
print(max_sum_array(arr))






